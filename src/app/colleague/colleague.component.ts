import {Component, OnInit} from '@angular/core';
import {ColleagueService} from '../services/colleague.service';
import {Colleague} from '../model/colleague.model';

@Component({
  selector: 'app-colleague',
  templateUrl: './colleague.component.html',
  styleUrls: ['./colleague.component.css']
})
export class ColleagueComponent implements OnInit {
  public colleagues: Colleague[];

  constructor( private readonly colleagueService: ColleagueService) {}

  ngOnInit() {
    this.getColleagues();
  }

  public getColleagues() {
    this.colleagueService.getColleagueData().subscribe(
      data => this.colleagues = data);
  }

}
