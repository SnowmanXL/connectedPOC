import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColleagueComponent } from './colleague.component';

describe('ColleagueComponent', () => {
  let component: ColleagueComponent;
  let fixture: ComponentFixture<ColleagueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColleagueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColleagueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
