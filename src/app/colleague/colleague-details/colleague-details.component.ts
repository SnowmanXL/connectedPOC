import {Component, Input, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Colleague} from '../../model/colleague.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-colleague-details',
  templateUrl: './colleague-details.component.html',
  styleUrls: ['./colleague-details.component.css']
})
export class ColleagueDetailsComponent implements OnInit {

  @Input() public colleague: Colleague;

  constructor(private modalService: NgbModal, private router: Router, private route: ActivatedRoute) {
  }

  open(content) {
    this.modalService.open(content);
  }

  ngOnInit() {
  }

  openShowcaseColleague() {
    this.router.navigate(['showcase/' + this.colleague.firstname + this.colleague.lastname], {relativeTo: this.route});

  }


}
