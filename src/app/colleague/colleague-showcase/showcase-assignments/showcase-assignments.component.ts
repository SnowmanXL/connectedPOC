import {Component, Input, OnInit} from '@angular/core';
import {ShowCase} from '../../../model/colleague.model';

@Component({
  selector: 'app-showcase-assignments',
  templateUrl: './showcase-assignments.component.html',
  styleUrls: ['./showcase-assignments.component.css']
})
export class ShowcaseAssignmentsComponent implements OnInit {

  @Input() public showcase: ShowCase;

  public showdetails = false;

  constructor() { }

  ngOnInit() {
  }

  public changeDetailsView() {
    console.log(this.showdetails);
    this.showdetails = !this.showdetails;
  }


}
