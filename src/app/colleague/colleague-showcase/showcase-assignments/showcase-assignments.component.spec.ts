import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcaseAssignmentsComponent } from './showcase-assignments.component';

describe('ShowcaseAssignmentsComponent', () => {
  let component: ShowcaseAssignmentsComponent;
  let fixture: ComponentFixture<ShowcaseAssignmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowcaseAssignmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowcaseAssignmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
