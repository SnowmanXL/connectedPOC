import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColleagueShowcaseComponent } from './colleague-showcase.component';

describe('ColleagueShowcaseComponent', () => {
  let component: ColleagueShowcaseComponent;
  let fixture: ComponentFixture<ColleagueShowcaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColleagueShowcaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColleagueShowcaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

