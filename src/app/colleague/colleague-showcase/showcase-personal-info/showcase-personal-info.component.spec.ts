import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcasePersonalInfoComponent } from './showcase-personal-info.component';

describe('ShowcasePersonalInfoComponent', () => {
  let component: ShowcasePersonalInfoComponent;
  let fixture: ComponentFixture<ShowcasePersonalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowcasePersonalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowcasePersonalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
