import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcaseCoursesComponent } from './showcase-courses.component';

describe('ShowcaseCoursesComponent', () => {
  let component: ShowcaseCoursesComponent;
  let fixture: ComponentFixture<ShowcaseCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowcaseCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowcaseCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
