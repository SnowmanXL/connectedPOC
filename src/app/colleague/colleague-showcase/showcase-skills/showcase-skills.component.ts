import {Component, Input, OnInit} from '@angular/core';
import {ShowCase} from '../../../model/colleague.model';

@Component({
  selector: 'app-showcase-skills',
  templateUrl: './showcase-skills.component.html',
  styleUrls: ['./showcase-skills.component.css']
})
export class ShowcaseSkillsComponent implements OnInit {

  @Input() public showcase: ShowCase;

  constructor() { }

  ngOnInit() {
  }



}
