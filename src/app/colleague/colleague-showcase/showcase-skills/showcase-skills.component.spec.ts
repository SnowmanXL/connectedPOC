import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcaseSkillsComponent } from './showcase-skills.component';

describe('ShowcaseSkillsComponent', () => {
  let component: ShowcaseSkillsComponent;
  let fixture: ComponentFixture<ShowcaseSkillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowcaseSkillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowcaseSkillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
