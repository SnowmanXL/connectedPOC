import {Component, HostListener, Inject, OnDestroy, OnInit} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {WINDOW} from '../../services/window.service';
import {ActivatedRoute, Data} from '@angular/router';
import {ColleagueService} from '../../services/colleague.service';
import {ShowCase} from '../../model/colleague.model';



@Component({
  selector: 'app-colleague-showcase',
  templateUrl: './colleague-showcase.component.html',
  styleUrls: ['./colleague-showcase.component.css']
})
export class ColleagueShowcaseComponent implements OnInit {
  public relative = false;
  public showcase: ShowCase = new ShowCase();


  ngOnInit(): void {
    console.log('oninit', Date.now());

    this.route.data.subscribe(
      (data: Data) => {
        this.showcase = data['showcase'];
      }
    );
  }

  constructor(@Inject(DOCUMENT) private document: Document
    , @Inject(WINDOW) private window
    , private route: ActivatedRoute
    , private readonly colleagueService: ColleagueService) {
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    const number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
    if (number > 535) {
      this.relative = true;
    } else if (this.relative && number < 535) {
      this.relative = false;
    }
  }

}

