import {AfterContentInit, Directive, ElementRef, HostListener, Inject, Renderer2} from '@angular/core';

import {DOCUMENT} from '@angular/common';

@Directive({
  selector: '[appScrollSpy]'
})
export class ScrollSpyDirective implements AfterContentInit {

  private elements = [];
  private currentActiveLink;
  private directNavigation = false;


  // TODO: Change the any type to Document when fix https://github.com/angular/angular/issues/15640
  constructor(@Inject(DOCUMENT) private document: any,
              private el: ElementRef,
              private renderer: Renderer2) {
  }

  public ngAfterContentInit(): void {
    this.collectIds();
  }


  private collectIds() {
    this.elements = [];
    let elements = this.el.nativeElement.querySelectorAll('a');

    for (let i = 0; i < elements.length; i++) {
      let elem = elements.item(i);

      let id = ScrollSpyDirective.getId(elem);
      if (!id)
        continue;

      let destination = this._getPeerElement(id);

      if (!destination)
        continue;

      elem.addEventListener('click', this._onLinkClicked.bind(this));

      this.elements.push({
        id,
        link: elem,
        destination
      });
    }
  }

  private _onLinkClicked(event: Event) {
    event.preventDefault();

    let target = event.currentTarget;
    let id = ScrollSpyDirective.getId(target);
    let destination = this._getPeerElement(id);
    this.directNavigation = true;

    let position = this.extractElementPosition(this.document, destination);

    window.scrollTo({top: position.top - 25, left: 0, behavior: 'smooth'});

    this._cleanCurrentLink();
    this._setCurrentLink(target);
    this.directNavigation = false;
  }

  private _getPeerElement(id) {

    let destination = this.document.getElementById(id);

    if (!destination) {
      return null;
    }


    return destination;
  }

  private static getId(elem) {
    let href = elem.getAttribute('href');

    if (!href) {
      return null;
    }


    return href.replace('#', '');
  }

  @HostListener("window:scroll", ['$event'])
  onWindowScroll(event: Event) {
    if (this.directNavigation)
      return;

    for (let elem of this.elements) {
      let top = elem.destination.getBoundingClientRect().top;
      if (top > 0 && top < 25) {
        this._cleanCurrentLink();
        this._setCurrentLink(elem.link);
        break;
      }
    }
  }

  private _cleanCurrentLink() {
    if (!this.currentActiveLink) {
      return;
    }


    this.renderer.removeClass(this.currentActiveLink, 'active');
  }

  private _setCurrentLink(elem) {
    this.currentActiveLink = elem;
    this.renderer.addClass(this.currentActiveLink, 'active');
  }


  private extractElementPosition(document: Document, scrollTargetElement: HTMLElement) {
    let body = document.body;
    let docEl = document.documentElement;

    let windowPageYOffset: number = document.defaultView && document.defaultView.pageYOffset || undefined;
    let windowPageXOffset: number = document.defaultView && document.defaultView.pageXOffset || undefined;

    let scrollTop = windowPageYOffset || docEl.scrollTop || body.scrollTop;
    let scrollLeft = windowPageXOffset || docEl.scrollLeft || body.scrollLeft;

    let clientTop = docEl.clientTop || body.clientTop || 0;
    let clientLeft = docEl.clientLeft || body.clientLeft || 0;


    if (this.isUndefinedOrNull(scrollTargetElement)) {
      // No element found, so return the current position to not cause any change in scroll position
      return {top: scrollTop, left: scrollLeft};
    }
    let box = scrollTargetElement.getBoundingClientRect();

    let top = box.top + scrollTop - clientTop;
    let left = box.left + scrollLeft - clientLeft;

    return {top: Math.round(top), left: Math.round(left)};
  }

  private isUndefinedOrNull(variable: any) {
    return (typeof variable === 'undefined') || variable === undefined || variable === null;
  }
}
