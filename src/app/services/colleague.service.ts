import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ColleagueService {

  constructor(private http: HttpClient) {
  }

  public getColleagueData(): Observable<any> {
    return this.http.get('./assets/colleague.stub.json');
  }

  public getShowcaseInfo(concatname: string): Observable<any> {
    return this.http.get('./assets/colleague.' + concatname + '.json');
  }
}
