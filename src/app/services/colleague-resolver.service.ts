import {ShowCase} from '../model/colleague.model';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {ColleagueService} from './colleague.service';
import {Injectable} from '@angular/core';

@Injectable()
export class ColleagueResolver implements Resolve<ShowCase> {

  constructor(private readonly colleagueService: ColleagueService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShowCase> | Promise<ShowCase> | ShowCase {
    return this.colleagueService.getShowcaseInfo(route.params['id'].toLowerCase());
  }

}
