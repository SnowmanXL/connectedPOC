import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ColleagueComponent} from './colleague/colleague.component';
import {ColleagueDetailsComponent} from './colleague/colleague-details/colleague-details.component';
import {ColleagueService} from './services/colleague.service';
import {HttpClientModule} from '@angular/common/http';
import {HomeComponent} from './home/home.component';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {ColleagueShowcaseComponent} from './colleague/colleague-showcase/colleague-showcase.component';
import {WINDOW_PROVIDERS} from './services/window.service';
import {ScrollSpyDirective} from './directives/scroll-spy.directive';
import {Ng2Timeline} from 'ng2-timeline';
import {ShowcaseAssignmentsComponent} from './colleague/colleague-showcase/showcase-assignments/showcase-assignments.component';
import {ShowcaseSkillsComponent} from './colleague/colleague-showcase/showcase-skills/showcase-skills.component';
import {ShowcaseCoursesComponent} from './colleague/colleague-showcase/showcase-courses/showcase-courses.component';
import {ShowcasePersonalInfoComponent} from './colleague/colleague-showcase/showcase-personal-info/showcase-personal-info.component';
import {ColleagueResolver} from './services/colleague-resolver.service';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'colleagues', component: ColleagueComponent},
  {path: 'colleagues/showcase/:id', component: ColleagueShowcaseComponent, resolve: {showcase: ColleagueResolver}}
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ColleagueComponent,
    ColleagueDetailsComponent,
    HomeComponent,
    ColleagueShowcaseComponent,
    ScrollSpyDirective,
    ShowcaseAssignmentsComponent,
    ShowcaseSkillsComponent,
    ShowcaseCoursesComponent,
    ShowcasePersonalInfoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false} // <-- debugging purposes only
    ),
    AngularSvgIconModule,
    Ng2Timeline
  ],
  providers: [
    ColleagueService,
    WINDOW_PROVIDERS,
    ColleagueResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
