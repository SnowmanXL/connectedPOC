export class Colleague {
  id: number;
  firstname: string;
  lastname: string;
  add: string;
  guild: string;
  manager: string;
  topfiveskill: {
    1: string;
    2: string;
    3: string;
    4: string;
    5: string;
  };
}

export class ShowCase {
  id: number;
  personalinfo: {
    firstname: string;
    lastname: string;
    add: string;
    guild: string;
    manager: string;
  };

  skills: {
    schooling: string[];
    topfiveskill: [
      { skill: string; level: number }
      ];
    skills: [
      { technical: [{ subject: string; skill: number }] },
      { agile: [{ subject: string; skill: number }] },
      { business: [{ subject: string; skill: number }] }
      ]
  };

  assignments: [{
    name: string;
    role: string;
    shortdescr: string;
    longdescr: string;
    periodfrom: string;
    periodto: string;
    duration: number;
  }];

  courses: [{
    subject: string;
    title: string;
    date: string;
  }];
}
